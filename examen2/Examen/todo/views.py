from django.shortcuts import render, redirect, get_object_or_404
from .models import Todo
from .forms import TodoForm

def todo_list_view(request):
    queryset = Todo.objects.all()
    items = list(queryset.values('id', 'title', 'user_id', 'completed'))
    return render(request, 'tasks/todo_list.html', {
        'items': items, 
        'headers': ['ID', 'Title', 'User ID', 'Completed', 'Actions'], 
        'show_actions': True
    })

def todo_list_id_view(request):
    queryset = Todo.objects.all().values('id')
    items = list(queryset)
    return render(request, 'tasks/todo_list.html', {'items': items, 'headers': ['ID'], 'show_actions': False})

def todo_list_title_view(request):
    queryset = Todo.objects.all().values('id', 'title')
    items = list(queryset)
    return render(request, 'tasks/todo_list.html', {'items': items, 'headers': ['ID', 'Title'], 'show_actions': False})

def todo_list_unresolved_view(request):
    queryset = Todo.objects.filter(completed=False).values('id', 'title')
    items = list(queryset)
    return render(request, 'tasks/todo_list.html', {'items': items, 'headers': ['ID', 'Title'], 'show_actions': False})

def todo_list_resolved_view(request):
    queryset = Todo.objects.filter(completed=True).values('id', 'title')
    items = list(queryset)
    return render(request, 'tasks/todo_list.html', {'items': items, 'headers': ['ID', 'Title'], 'show_actions': False})

def todo_list_user_id_view(request):
    queryset = Todo.objects.all().values('id', 'user_id')
    items = list(queryset)
    return render(request, 'tasks/todo_list.html', {'items': items, 'headers': ['ID', 'User ID'], 'show_actions': False})

def todo_list_resolved_user_id_view(request):
    queryset = Todo.objects.filter(completed=True).values('id', 'user_id')
    items = list(queryset)
    return render(request, 'tasks/todo_list.html', {'items': items, 'headers': ['ID', 'User ID'], 'show_actions': False})

def todo_list_unresolved_user_id_view(request):
    queryset = Todo.objects.filter(completed=False).values('id', 'user_id')
    items = list(queryset)
    return render(request, 'tasks/todo_list.html', {'items': items, 'headers': ['ID', 'User ID'], 'show_actions': False})

def todo_create_view(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todo-list')
    else:
        form = TodoForm()
    return render(request, 'tasks/todo_form.html', {'form': form})

def index_view(request):
    return render(request, 'tasks/index.html')

def todo_toggle_complete_view(request, pk):
    todo = get_object_or_404(Todo, pk=pk)
    todo.completed = not todo.completed
    todo.save()
    return redirect(request.META.get('HTTP_REFERER', 'todo-list'))
