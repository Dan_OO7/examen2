from django.urls import path
from .views import (
    todo_list_view, 
    todo_list_id_view, 
    todo_list_title_view, 
    todo_list_unresolved_view, 
    todo_list_resolved_view, 
    todo_list_user_id_view, 
    todo_list_resolved_user_id_view, 
    todo_list_unresolved_user_id_view,
    todo_create_view,
    index_view,
    todo_toggle_complete_view
)

urlpatterns = [
    path('', index_view, name='index'),
    path('todos/', todo_list_view, name='todo-list'),
    path('todos/ids/', todo_list_id_view, name='todo-list-id'),
    path('todos/titles/', todo_list_title_view, name='todo-list-title'),
    path('todos/unresolved/', todo_list_unresolved_view, name='todo-list-unresolved'),
    path('todos/resolved/', todo_list_resolved_view, name='todo-list-resolved'),
    path('todos/user_ids/', todo_list_user_id_view, name='todo-list-user-id'),
    path('todos/user_ids/resolved/', todo_list_resolved_user_id_view, name='todo-list-resolved-user-id'),
    path('todos/user_ids/unresolved/', todo_list_unresolved_user_id_view, name='todo-list-unresolved-user-id'),
    path('todos/create/', todo_create_view, name='todo-create'),
    path('todos/<int:pk>/toggle-complete/', todo_toggle_complete_view, name='todo-toggle-complete'),
]
